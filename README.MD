# TASK MANAGER

## DEVELOPER INFO
* **Name**: Daria Kozoriz
* **E-mail**: dkozoriz@t1-consulting.ru
* **E-mail**: kdv1996@mail.ru

## SOFTWARE

* **OS**: Windows 10

* **JAVA**: OpenJDK 1.8.0_345

## HARDWARE

* **CPU**: i7
* **RAM**: 16GB
* **SSD**: 459GB

## PROGRAM RUN

```shell
java -jar ./tm.jar
```
